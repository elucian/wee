## Composite Types

Composite data types are user defined data types.

* [Ordinal](#ordinal)
* [Array](#array)
* [Slice](#slice)
* [Object](#object)
* [String](#string)

## Ordinal

Is an enumeration of symbols ordered by position. 

```
** create ordinal type
def type_name : {symbol, ...};

** create a variable with initial value one identifier
new new_name  : type_name.symbol;
```

**Note:** 
* Ordinal is a numeric type,
* Ordinal support numeric operators,
* Each symbol has a numeric value,
* First symbol has value = 0.


## Array

Array has a capacity and a data type.

**array_type**

```
new array_name : [type](capacity)
let array_name : [1,2,3,4,5] ; array literal 
```

## Slice

Slice is a section from an array.

```
new array_name :[0](100) ; array of 100 numbers
new slice_name :array_name[0..10] ; slice of first 10 
```

## Object

* Objects are name:value pairs separated by comma. 
* Each element can have a different data type.

```
def class_name  :{name:type, ...}
new object_name :{name:value, ...}
```

**Note:** Empty object is represented like this: {}

## String

Strings are arrays of ASCII symbols:

**syntax:**
```
new string_name : [''](capacity)
```

A string literal is enclosed in double quotes: ""

**example**

```
** string with capacity of 20 symbols
new test : [''](20) 

** initialize the string with literals
let test : "this is a string"
```

**Go back:** [readme.md](readme.md)