## Wee operators

In Wee operators are created one ASCII symbol.

## Brackets

Wee is using brackets for expressions and data structures.

symbol| description
------|-----------------------------------------------------
  `()`| Comma separated enumeration of values
  `[]`| Array \| Slice \| Subscript
  `{}`| Multi-line comments

## Delimiters

symbol    | description
----------|----------------------------------------------------------------
 `'...'`  | ASCII string literal 
 `"..."`  | Unicode string literal
 `+-.-+`  | Multi-line comments

## Single Symbols

symbol| description
------|----------------------------------------------------------------
 `^`  | Power symbol used with fractions or expressions
 `#`  | Template placeholder
 `$`  | Global constant prefix \| Environment variables
 `@`  | Global variable prefix \| Wee global variables
 `?`  | Query memory address \| Generate an integer
 `:`  | Assign value \| Copy value
 `=`  | Equivalent \| Equal
 `;`  | End of line comment
 `.`  | Membership qualifier notation 
 `*`  | Multiplication
 `+`  | Numeric addition 
 `-`  | Numeric subtraction 

## Two Symbols

symbol| description
------|----------------------------------------------------------------
 `--` | Single line comment or indented comment
 `+:` | Addition modifier \| Pointer increment
 `-:` | Subtraction modifier \| Pointer decrement

## Logic operators

Logical operators return: 1 = true or 0 = false

symbol| meaning | arity
------|---------|-------------------------------
  `!` | NOT     | unary 
  `~` | XOR     | dual 
  `&` | AND     | dual 
 `\|` | OR      | dual 
  `=` | EQ      | dual   
  `>` | GT      | dual
  `<` | LT      | dual

**patterns:** 
```
!(x = y) ; divergence of two native values
!(x > y) ; equivalent (x <= y)
!(x < y) ; equivalent (x >= y)
```

**The table of through**

 p | q | !p |p ~ q |p = q |p & q | p \| q
---|---|----|------|------|------|--------
 1 | 1 | 0  | 0    | 1    |1     | 1      
 1 | 0 | 0  | 1    | 0    |0     | 1      
 0 | 1 | 1  | 1    | 0    |0     | 1      
 0 | 0 | 1  | 0    | 1    |0     | 0      

**Bit Manipulation**

 symbol | description
--------|----------------------------------
  `<<`  | shift bits to left  
  `>>`  | shift bits to right


 A    | B   | A & B  | A \| B  | A ~ B
------|-----|--------|---------|--------
 00   | 00  | 00     | 00      |  11    
 01   | 00  | 00     | 01      |  10    
 11   | 01  | 01     | 11      |  00    
 10   | 11  | 10     | 11      |  01    


 A    | !A  | A\<\<1| A\>\>2  
------|-----|-------|--------
 0000 |1111 | 0000  | 0000   
 1111 |0000 | 1110  | 0011   
 0111 |1000 | 1110  | 0001   
 0110 |1001 | 1100  | 0001   

See also:[Bit Manipulation](https://en.wikipedia.org/wiki/Bit_manipulation) 

* Go back: [readme.md](readme.md)     
* Read Next: [overview](overview.md)
