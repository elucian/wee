## Wee keywords

Wee uses 26 English reserved keywords:

1. if, is, in, or, go, do 
1. use, new, let, get, put, say, def, out, log  
1. case. spin, more, node, else, done, back, share, clear 
1. run, loop, over

**content**
* [input/output](#io)
* [declaration](#declaration)
* [expression](#expression)
* [conditional](#conditional)
* [repetition](#repetition)
* [subroutine](#subroutine)

## Import
To find public members in other files: Wee create this statement:

```
use folder.name/file_name.wee
...
```

Public members start with capital letter.

## IO
For input output wee uses console and 3 statements:

* out ;write something to console
* say ;write something to console and new line
* ask ;print something and wait for input

## Declaration
Wee is typeless, or more precisely has one data type: the computer word. Sometimes this represent a computer address that has to be dereferenced. Capacity of word is 16 bit, 32 bit or 64 bit depending on the operating system.

**syntax**
```
new identifier : constant   ; using constant literal
new identifier : expression ; using expression

** define several identifiers with same initial value
new identifier, identifier,... :expression 
```

**example**
```
new v : 0  
new x,y,z : 10
```

**Note:** 

* Wee data types are aligned to memory address: 00000000-FFFFFFFF
* Wee core library can not handle decimal numbers nor division

## Expression

Expressions can be used for assign operator `:` or modifiers: `+:, -:` 

```
new v  : 10 ;number 
new s  : 5  ;number

let v +: 1  ;make v = 11
let v -: 1  ;make v = 10
let s  : v + s; copy v = 15 to s
```

**Note:** 
* Operator "=" is used as logic operator;
* Types are defined using type inference;

## Pointer

Wee is using pointers. You can dereference a pointer using symbol "@"

```
new v : 10 ; define a new variable
new p : @v ; define a reference to v

say p ; expected 10

** using assign ":"
let  v : 2; change underline value of p 
say ?p ; expected 2

** reset pointer
new o : 4
let p : @o ; reset
say p ; expected 4
say v ; expected 2 (unmodified)
```

## Console

The console application accept and print characters:

```
ask i : "enter any char"
say i ; will print the char
```

## Conditional

Conditional _if_ statement has a postfix form:

```
  statement if condition
```

Ternary expression;
```
let new_variable : (value if condition, 
                    value if condition, 
                    value);
```

## Selector

A selector is using one condition to create a fork:

```
when condition do
  ** true
else 
  ** false
done
```

## Case

A case is a multi-path selector that is using multiple conditions:

```
case condition do
  ** first path
  ...  
case condition do
  ** second path
  ...
else
  ** alternative path  
  ...
done
```

## Repetition

Repetitive block is realized using a spin block:

**repetitive spin**

```
spin
  ...
  loop if (condition)
  ...
  over if (condition)
more
```
**note**
* Spin block can not be nested inside another spin block;
* Statement loop is optional but statement over is mandatory,

## Subroutine

Node is a named block of code with local context similar to a subroutine:

```
node node_name go  
  get a, b, c : 0  
  put a + b + c    
back
```

## Private context

A private context is an anonymous block of code used to run a node;

```
share
  new r : 0
  put a : 5, b : 10, c : 20  
  run node_name 
  pop r 
  say r
clear
```

Using outer variables is possible in private context.

```
new r : 0
share
  put a : 5, b : 10, c : 20  
  run node_name 
  pop r 
clear  ;a, b, c 
say r  ;a+b+c = 35
```

* Go back: [readme.md](readme.md)   
* Read next: [composite.md](composite.md)   