# Wee Lang

A technical language with minimal functionality.

**Features:**

1. free form language
1. direct memory access
1. manual memory management
1. console only I/O
1. single source file extension: *.wee
1. infix arithmetic expressions
1. addition, subtraction, multiplication
1. bitwise operations
1. single line comments
1. multiple line comments

**Restrictions:**

1. no garbage collector
1. no reference counting
1. no exception handlers
1. no type system
1. no disk access
1. no ABI interface
1. no division operator
1. no functions
1. no Unicode support
1. no floating point numbers

**Doc**

* [readme.md](doc/readme.md)


**Examples**
Examples are in folder: test.

* [node](test/node.wee)
* [pointer](test/pointer.wee)

Copyright (c) 2018 Elucian Moise